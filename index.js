var mocha = require('mocha')
var pug = require('pug')
var fs = require('fs')
var PNG = require('pngjs').PNG
var crypto = require('crypto')

module.exports = PixelPReporter

function PixelsNotPerfectError(expected, actual, message) {
  this.name = 'PixelsNotPerfectError'
  this.message = message || (expected + ' is not ' + actual)
  this.stack = (new Error()).stack
	this.expected = expected
	this.actual = actual
}
PixelsNotPerfectError.prototype = Object.create(Error.prototype)
PixelsNotPerfectError.prototype.constructor = PixelsNotPerfectError


function PixelPReporter(runner) {
	var base = new mocha.reporters.List(runner)

	var tests = []

	runner.on('test end', function (test) {
		tests.push(test)
	})

	runner.on('end', function () {
		try {
			var html = pug.renderFile(__dirname + '/report.pug', { 
				tests: tests,
				skip: base.stats.pending,
				pass: base.stats.passes,
				fail: base.stats.failures,
			})
			fs.writeFileSync('./.pixels/report.html', html)
		} catch (err) {
			console.error(err)
		}
	})

	return base
}


function assertPixels(driver, hash, element, message) {

	function getPosition(element) {
		if (!element) { return Promise.resolve(null) }
		return driver.findElement(element)
			.then(e => Promise.all([e.getLocation(), e.getSize()]) )
			.then(r => ({
				x: r[0].x,
				y: r[0].y,
				width: r[1].width,
				height: r[1].height,
			}))
	}

	return driver.takeScreenshot()
		.then(data => {
			var buffer = new Buffer(data, 'base64')
			return getPosition(element).then(position => {
				if ( position ) {
					var png = PNG.sync.read(buffer)
					var elementPng = new PNG({width: position.width, height: position.height})
					PNG.bitblt(png, elementPng, position.x, position.y, position.width, position.height, 0, 0)
					buffer = PNG.sync.write(elementPng)
				}
				newHash = crypto.createHash('sha1').update(buffer).digest('hex')

				try { fs.mkdirSync('./.pixels') } catch (e) {}
				fs.writeFileSync('./.pixels/' + newHash + '.png', buffer)

				if ( hash !== newHash ) {
					throw new PixelsNotPerfectError(hash, newHash, message)
				}
			})
		})
}


PixelPReporter.assertPixels = assertPixels